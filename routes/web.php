<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ProductController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\ProductCategoryController;
use App\Http\Controllers\OptionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'root']);

Route::resources([
    'products' => ProductController::class,
    'brands' => BrandController::class,
    'product-categories' => ProductCategoryController::class,
    'options' => OptionController::class,
]);

Route::match(['get', 'post'], '/products/search', [ProductController::class, 'search'])->name('products.search');
Route::post('/products/{product}/toggleFeatured', [ProductController::class, 'toggleFeatured'])->name('products.toggleFeatured');
Route::delete('/products/{productMedia}/removeMedia', [ProductController::class, 'removeMedia'])->name('products.removeMedia');
Route::delete('/products/{productVariant}/removeVariant', [ProductController::class, 'removeVariant'])->name('products.removeVariant');
// Route::post('/products/{product}/clone', [ProductController::class, 'clone'])->name('products.clone');


Route::get('{any}', [App\Http\Controllers\HomeController::class, 'index']);
//Language Translation
Route::get('index/{locale}', [App\Http\Controllers\HomeController::class, 'lang']);
Route::post('/formsubmit', [App\Http\Controllers\HomeController::class, 'FormSubmit'])->name('FormSubmit');