<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = [
        'name',
        'sku',
        'slug',
        'price',
        'price_without_tax',
        'brand_id',
        'weight_kg',
        'description',
        'quantity',
        'quantity_low_mark',
        'fixed_shipping_fee',
        'is_free_shipping',
        'page_meta_title',
        'page_meta_description',
        'is_featured',
        'is_active',
        'created_by',
    ];
    
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * Get the brand associated with the product.
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    /**
     * The cateories that belong to the product.
     */
    public function categories()
    {
        return $this->belongsToMany(ProductCategory::class, 'products_product_categories', 'product_id', 'product_category_id');
    }

    /**
     * Get the media for the product.
     */
    public function media()
    {
        return $this->hasMany(ProductMedia::class);
    }

    /**
     * Get the variants for the product.
     */
    public function variants()
    {
        return $this->hasMany(Variant::class);
    }
}
