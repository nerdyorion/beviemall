<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'value',
        'is_active',
        'created_by',
    ];

    /**
     * The variants that belong to the option.
     */
    public function variants()
    {
        return $this->belongsToMany(Variant::class, 'variant_option');
        // return $this->belongsToMany(Variant::class, 'variant_option', 'option_id', 'variant_id');
    }
}
