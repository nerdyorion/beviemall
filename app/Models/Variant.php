<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variant extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'selection_type',
        'color_hex',
        'price',
        'is_default',
        'created_by',
    ];

    /**
     * Get the product that owns the variant.
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * The options that belong to the variant.
     */
    public function options()
    {
        return $this->belongsToMany(Option::class, 'variant_option');
        // return $this->belongsToMany(Option::class, 'variant_option', 'variant_id', 'option_id');
    }
}
