<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class BrandController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Brand::paginate(config("app.pagination_size"));
        return view('brands.index')->with(compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:App\Models\Brand|max:255',
            'short_name' => 'nullable',
            'image_url' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'is_active' => 'boolean',
        ]);

        $image_url = null;
        if($request->hasFile('image_url')) {
            // Get Filename
            $filenameWithExt = $request->file('image_url')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $filename = $filename . '_'.  time() . '.' . $request->image_url->extension(); 

            // set image url as path and filename
            $image_url = str_replace("public/", "storage/", config("app.brands_media_dir")) . '/' . $filename;

            // save file to path
            $request->file('image_url')->storePubliclyAs(config("app.brands_media_dir"), $filename);

            // generate thumbnails
        }

        $brand = Brand::create([
            'name' => $validated["name"],
            'short_name' => $validated["short_name"],
            'image_url' => $image_url,
            'is_active' => $validated["is_active"] ?? false,
            'created_by' => Auth::user()->id,
        ]);

        return redirect()->route('brands.index')->with('status', config("app.success_message"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return view('brands.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('brands.edit')->with(compact("brand"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $validated = $request->validate([
            'name' => 'required|unique:App\Models\Brand,name,'.$brand->id .'|max:255',
            'short_name' => 'nullable',
            'image_url' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'is_active' => 'boolean',
        ]);

        $image_url = $brand->image_url;
        if($request->hasFile('image_url')) {
            $filenameWithExt = $request->file('image_url')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $filename = $filename . '_'.  time() . '.' . $request->image_url->extension(); 

            // set image url as path and filename
            $new_image_url = str_replace("public/", "storage/", config("app.brands_media_dir")) . '/' . $filename;
            
            // save file to path
            $request->file('image_url')->storePubliclyAs(config("app.brands_media_dir"), $filename);
            
            // delete former image
            $this->deleteImage($image_url);

            $image_url = $new_image_url;
        }

        $brand->name = $validated["name"];
        $brand->short_name = $validated["short_name"] ?? $brand->short_name;
        $brand->image_url = $image_url;
        $brand->is_active = $validated["is_active"] ?? false;
        $brand->save();

        return redirect()->route('brands.index')->with('status', config("app.success_message"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        // delete former image
        $this->deleteImage($brand->image_url);

        // delete model
        $brand->delete();
        
        return redirect()->route('brands.index')->with('status', config("app.success_message"));
    }

    private function deleteImage(?string $url) {
        if($url) {
            Storage::disk('public')->delete(str_replace("storage/", "", $url));
        }
    }
}
