<?php

namespace App\Http\Controllers;

use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ProductCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = ProductCategory::paginate(config("app.pagination_size"));
        // $data = ProductCategory::with('children')->whereNull('parent_id')->get();
        $data = ProductCategory::with('children')->whereNull('parent_id')->paginate(config("app.pagination_size"));

        return view('product-categories.index')->with(compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $data["categories"] = ProductCategory::where('is_active', 1)->orderBy('name')->pluck('name', 'id');
        $data["categories"] = ProductCategory::with('children')->whereNull('parent_id')->orderBy('name')->get();
        return view('product-categories.create')->with(compact("data"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:App\Models\ProductCategory|max:255',
            'parent_id' => 'nullable',
            'image_url' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'is_active' => 'boolean',
        ]);

        $image_url = null;
        if($request->hasFile('image_url')) {
            // Get Filename
            $filenameWithExt = $request->file('image_url')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $filename = $filename . '_'.  time() . '.' . $request->image_url->extension(); 

            // set image url as path and filename
            $image_url = str_replace("public/", "storage/", config("app.categories_media_dir")) . '/' . $filename;

            // save file to path
            $request->file('image_url')->storePubliclyAs(config("app.categories_media_dir"), $filename);

            // generate thumbnails
        }

        $productCategory = ProductCategory::create([
            'name' => $validated["name"],
            'parent_id' => $validated["parent_id"],
            'image_url' => $image_url,
            'is_active' => $validated["is_active"] ?? false,
            'created_by' => Auth::user()->id,
        ]);

        return redirect()->route('product-categories.index')->with('status', config("app.success_message"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        return view('product-categories.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductCategory $productCategory)
    {
        // $data["categories"] = ProductCategory::where('is_active', 1)->where('id', '!=', $productCategory->id)
        // ->orderBy('name')->pluck('name', 'id');
        $data["categories"] = ProductCategory::with('children')->where('is_active', 1)->where('id', '!=', $productCategory->id)
        ->orderBy('name')->whereNull('parent_id')->get();

        return view('product-categories.edit')->with(compact("productCategory", "data"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductCategory $productCategory)
    {
        $validated = $request->validate([
            'name' => 'required|unique:App\Models\ProductCategory,name,'.$productCategory->id .'|max:255',
            'parent_id' => 'nullable',
            'image_url' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'is_active' => 'boolean',
        ]);

        $image_url = $productCategory->image_url;
        if($request->hasFile('image_url')) {
            $filenameWithExt = $request->file('image_url')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $filename = $filename . '_'.  time() . '.' . $request->image_url->extension(); 

            // set image url as path and filename
            $new_image_url = str_replace("public/", "storage/", config("app.categories_media_dir")) . '/' . $filename;
            
            // save file to path
            $request->file('image_url')->storePubliclyAs(config("app.categories_media_dir"), $filename);
            
            // delete former image
            $this->deleteImage($image_url);

            $image_url = $new_image_url;
        }

        $productCategory->name = $validated["name"];
        $productCategory->parent_id = $validated["parent_id"];
        $productCategory->image_url = $image_url;
        $productCategory->is_active = $validated["is_active"] ?? false;
        $productCategory->save();

        return redirect()->route('product-categories.index')->with('status', config("app.success_message"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductCategory $productCategory)
    {
        // delete former image
        $this->deleteImage($productCategory->image_url);

        // delete model
        $productCategory->delete();

        return redirect()->route('product-categories.index')->with('status', config("app.success_message"));
    }

    private function deleteImage(?string $url) {
        if($url) {
            Storage::disk('public')->delete(str_replace("storage/", "", $url));
        }
    }
}
