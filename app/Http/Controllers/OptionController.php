<?php

namespace App\Http\Controllers;

use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class OptionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Option::paginate(config("app.pagination_size"));
        return view('options.index')->with(compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('options.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|max:255',
            'value' => 'required|max:255',
            'is_active' => 'boolean',
        ]);

        $option = Option::create([
            'name' => $validated["name"],
            'value' => $validated["value"],
            'is_active' => $validated["is_active"] ?? false,
            'created_by' => Auth::user()->id,
        ]);

        return redirect()->route('options.index')->with('status', config("app.success_message"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function show(Option $option)
    {
        return view('options.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function edit(Option $option)
    {
        return view('options.edit')->with(compact("option"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Option $option)
    {
        $validated = $request->validate([
            'name' => 'required|max:255',
            'value' => 'required|max:255',
            'is_active' => 'boolean',
        ]);

        $option->name = $validated["name"];
        $option->value = $validated["value"];
        $option->is_active = $validated["is_active"] ?? false;
        $option->save();

        return redirect()->route('options.index')->with('status', config("app.success_message"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Option  $option
     * @return \Illuminate\Http\Response
     */
    public function destroy(Option $option)
    {
        // delete model
        $option->delete();
        
        return redirect()->route('options.index')->with('status', config("app.success_message"));
    }
}
