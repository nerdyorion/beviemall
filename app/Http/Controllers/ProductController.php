<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductMedia;
use App\Models\Brand;
use App\Models\Variant;
use App\Models\Option;
use App\Models\PriceHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Product::paginate(config("app.pagination_size"));
        return view('products.index')->with(compact("data"));
    }

    private function group_assoc($array, $key) {
        $return = array();
        foreach($array as $v) {
            $return[$v[$key]][] = $v;
        }
        return $return;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data["brands"] = Brand::where('is_active', 1)->orderBy('name')->pluck('name', 'id');
        // $data["options"] = Brand::where('is_active', 1)->orderBy('name')->pluck('name', 'id');
        $data["options"] = Option::where('is_active', 1)->orderBy('name')->get();

        // Group the options value by their name
        $options = $this->group_assoc($data["options"], 'name');

        // dd($request->clone);
        $cloned_product = null;

        if($request->clone) {
            $cloned_product = Product::where('id', $request->clone)->first();

            $cloned_product->name = 'Copy of '. $cloned_product->name;

            $selected = $cloned_product->categories->map(function ($category, $key) {
                return $category->id;
            });
            $cloned_product->categories = $selected->all();


            foreach($cloned_product->variants as $key => $variant) {
                $variant_options = [];
                $variant_options = $variant->options->map(function ($option, $key) {
                    return $option->id;
                });
                $cloned_product->variants[$key]->options = $variant_options->all();
            }
        }

        $data["categories"] = ProductCategory::with('children')->where('is_active', 1)->whereNull('parent_id')->orderBy('name')->get();
        // dd($data["brands"]);
        return view('products.create')->with(compact("data", "options", "cloned_product"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:App\Models\Product|max:2000',
            'sku' => 'required|unique:App\Models\Product|max:50',
            'image_url.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price' => 'required',
            'price_without_tax' => 'required',
            'slug' => 'nullable',
            'brand_id' => 'nullable',
            'weight_kg' => 'required',
            'description' => 'nullable',
            'quantity' => 'required',
            'quantity_low_mark' => 'nullable',
            'fixed_shipping_fee' => 'nullable',
            'is_free_shipping' => 'boolean',
            'page_meta_title' => 'nullable',
            'page_meta_description' => 'nullable',
            'is_featured' => 'boolean',
            'is_active' => 'boolean',
        ]);

        $product = Product::create([
            'name' => $validated["name"],
            'sku' => $validated["sku"],
            'price' => $validated["price"],
            'price_without_tax' => $validated["price_without_tax"],
            'weight_kg' => $validated["weight_kg"],
            'description' => $validated["description"],
            'quantity' => $validated["quantity"],
            'quantity_low_mark' => $validated["quantity_low_mark"],
            'fixed_shipping_fee' => $validated["fixed_shipping_fee"],
            'is_free_shipping' => $validated["is_free_shipping"] ?? false,
            'page_meta_title' => $validated["page_meta_title"],
            'page_meta_description' => $validated["page_meta_description"],
            'is_featured' => $validated["is_featured"] ?? false,
            'is_active' => $validated["is_active"] ?? false,
            'created_by' => Auth::user()->id,
        ]);

        // save images
        $this->saveProductMedia($request, $product->id);

        if($validated["brand_id"]) {
            // add brand
            $brand = Brand::findOrFail($validated["brand_id"]);
            $product->brand()->associate($brand);
            $product->save();
        }

        // add categories
        if(isset($request->categories) && count($request->categories)) {
            // dd($request->categories);
            $product->categories()->attach($request->categories);
        }

        // add variants
        if(isset($request->variant_options) && count($request->variant_options)) {
            // dd($request->variant_options);

            foreach($request->variant_options as $key => $variant_options) {

                $variant_options_arr = explode(",", $variant_options);

                if(!array_filter($variant_options_arr)) {
                    // empty fields
                } else {
                    // create variant
                    $variant = Variant::create([
                        'product_id' => $product->id,
                        'selection_type' => $request->variant_selection_types[$key] ?? '',
                        'color_hex' => $request->variant_color_hexes[$key] ?? null,
                        'price' => $request->variant_prices[$key] ?? null,
                        'is_default' => $request->variant_is_defaults[$key] ?? false,
                        'created_by' => Auth::user()->id,
                    ]);

                    // attach variant options
                    $variant->options()->attach($variant_options_arr);
                }
            }
        }

        return redirect()->route('products.index')->with('status', config("app.success_message"));
    }

    /**
     * Clone a resource in storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function clone(Product $product)
    {
        // create new product
        $new_product = Product::create([
            'name' => 'Copy of ' . $product->name,
            'brand_id' => $product->brand_id,
            'sku' => $product->sku . '_' . uniqid(),
            'price' => $product->price,
            'price_without_tax' => $product->price_without_tax,
            'weight_kg' => $product->weight_kg,
            'description' => $product->description,
            'quantity' => $product->quantity,
            'quantity_low_mark' => $product->quantity_low_mark,
            'fixed_shipping_fee' => $product->fixed_shipping_fee,
            'is_free_shipping' => $product->is_free_shipping,
            'page_meta_title' => $product->page_meta_title,
            'page_meta_description' => $product->page_meta_description,
            'is_featured' => $product->is_featured,
            'is_active' => $product->is_active,
            'created_by' => Auth::user()->id,
        ]);

        // clone images: fetch images and duplicate
        foreach($product->media as $media) {

            $new_image_url = null;

            if($media->media_url) {

                $append_time = '_' . uniqid();
                // str_replace("public/", "storage/", $media->media_url);

                $image_url_arr = explode(".", $media->media_url);
                $extension = array_pop($image_url_arr);
                $tmp_image_url = implode(".", $image_url_arr);

                $new_image_url = $tmp_image_url . $append_time . '.' . $extension;

                // copy image
                if(Storage::disk('public')->exists(str_replace("storage/", "", $media->media_url)) ) {
                    \File::copy($media->media_url, public_path() . "/" . $new_image_url);
                }
            }

            // save media
            $product_media = ProductMedia::create([
                'product_id' => $new_product->id,
                'media_type' => $media->media_type,
                'media_url' => $new_image_url,
                'is_main_image' => $media->is_main_image,
                'created_by' => Auth::user()->id,
            ]);
        }

        $categories = [];
        $categories = $product->categories->map(function ($category, $key) {
            return $category->id;
        });

        // add categories
        if(count($categories)) {
            $new_product->categories()->attach($categories);
        }

        // add variants
        foreach($product->variants as $variant) {
            
            $variant_options = [];
            $variant_options = $variant->options->map(function ($option, $key) {
                return $option->id;
            });

            // create variant
            $new_variant = Variant::create([
                'product_id' => $new_product->id,
                'selection_type' => $variant->selection_type,
                'color_hex' => $variant->color_hex,
                'price' => $variant->price,
                'is_default' => $variant->is_default,
                'created_by' => Auth::user()->id,
            ]);

            // attach variant options
            if(count($variant_options)) {
                $new_variant->options()->attach($variant_options);
            }
        }

        return redirect()->route('products.index')->with('status', config("app.success_message"));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $data["brands"] = Brand::where('is_active', 1)->orderBy('name')->pluck('name', 'id');
        $data["categories"] = ProductCategory::with('children')->where('is_active', 1)->whereNull('parent_id')->orderBy('name')->get();

        $selected = $product->categories->map(function ($category, $key) {
            return $category->id;
        });
        $selected_categories = $selected->all();

        $data["options"] = Option::where('is_active', 1)->orderBy('name')->get();

        // Group the options value by their name
        $options = $this->group_assoc($data["options"], 'name');

        return view('products.edit')->with(compact("product", "data", "selected_categories", "options"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validated = $request->validate([
            'name' => 'required|unique:App\Models\Product,name,'.$product->id .'|max:2000',
            'sku' => 'required|unique:App\Models\Product,sku,'.$product->id .'|max:50',
            'image_url.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'price' => 'required',
            'price_without_tax' => 'required',
            'slug' => 'nullable',
            'brand_id' => 'nullable',
            'weight_kg' => 'required',
            'description' => 'nullable',
            'quantity' => 'required',
            'quantity_low_mark' => 'nullable',
            'fixed_shipping_fee' => 'nullable',
            'is_free_shipping' => 'boolean',
            'page_meta_title' => 'nullable',
            'page_meta_description' => 'nullable',
            'is_featured' => 'boolean',
            'is_active' => 'boolean',
        ]);

        $former_price = $product->price;
        $price_has_changed = ($former_price != $validated["price"]);

        $product->brand()->dissociate();
        if($validated["brand_id"]) {
            // add brand
            $brand = Brand::findOrFail($validated["brand_id"]);
            $product->brand()->associate($brand);
            $product->save();
        }
        // add brand

        $product->name = $validated["name"];
        $product->sku = $validated["sku"];
        $product->price = $validated["price"];
        $product->price_without_tax = $validated["price_without_tax"];
        $product->weight_kg = $validated["weight_kg"];
        $product->description = $validated["description"];
        $product->quantity = $validated["quantity"];
        $product->quantity_low_mark = $validated["quantity_low_mark"];
        $product->fixed_shipping_fee = $validated["fixed_shipping_fee"];
        $product->is_free_shipping = $validated["is_free_shipping"] ?? false;
        $product->page_meta_title = $validated["page_meta_title"];
        $product->page_meta_description = $validated["page_meta_description"];
        $product->is_featured = $validated["is_featured"] ?? false;
        $product->is_active = $validated["is_active"] ?? false;
        $product->save();

        if($price_has_changed)
        {
            // add to price history if price is changed
            $price_history = PriceHistory::create([
                'product_id' => $product->id,
                'former_price' => $former_price,
                'new_price' => $product->price,
                'created_by' => Auth::user()->id,
            ]);
            $price_history->save();
        }

        // save images
        $this->saveProductMedia($request, $product->id);

        // update categories
        if(isset($request->categories) && count($request->categories)) {
            $product->categories()->sync($request->categories);
        }
        else {
            $product->categories()->detach();
        }

        // add variants
        if(isset($request->variant_options) && count($request->variant_options)) {
            // dd($request->variant_options);
            $new_default_variant_id = '';
            foreach($request->variant_options as $key => $variant_options) {

                $variant_options_arr = explode(",", $variant_options);

                if(!array_filter($variant_options_arr)) {
                    // empty fields
                } else {                
                    // create variant
                    $variant = Variant::create([
                        'product_id' => $product->id,
                        'selection_type' => $request->variant_selection_types[$key] ?? '',
                        'color_hex' => $request->variant_color_hexes[$key] ?? null,
                        'price' => $request->variant_prices[$key] ?? null,
                        'is_default' => $request->variant_is_defaults[$key] ?? false,
                        'created_by' => Auth::user()->id,
                    ]);

                    if(isset($request->variant_is_defaults[$key]) && $request->variant_is_defaults[$key]) {
                        $new_default_variant_id = $variant->id;
                    }

                    // attach variant options
                    $variant->options()->attach($variant_options_arr);
                }
            }

            if($new_default_variant_id) {
                Variant::where('product_id', $product->id)->where('id', '!=', $new_default_variant_id)
                    ->update(['is_default' => 0]);
            }
        }
        

        return redirect()->route('products.index')->with('status', config("app.success_message"));
    }

    /**
     * Display a search form for the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        return view('products.search');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // delete all product images/videos
        foreach ($product->media as $item) {
            $this->deleteImage($item->media_url);
        }

        // remove mappings in pivot
        $product->categories()->detach();

        // delete variants
        foreach($product->variants as $variant) {
            $variant->options()->detach();
            $variant->delete();
        }

        // delete model
        $product->delete();

        return redirect()->route('products.index')->with('status', config("app.success_message"));
    }

    /**
     * Removes the specified resource from storage.
     *
     * @param  \App\Models\ProductMedia  $productMedia
     * @return \Illuminate\Http\Response
     */
    public function removeMedia(ProductMedia $productMedia)
    {
        try
        {
            $delete_id = $productMedia->id;
            $product_id = $productMedia->product->id;

            // delete all product image/video
            $this->deleteImage($productMedia->media_url);

            // delete model
            $productMedia->delete();

            // set main image
            $this->setMainImage($product_id);
            
            echo 1;
        }
        catch(Exception $e)
        {
            echo 0;
        }
    }

    /**
     * Removes the specified resource from storage.
     *
     * @param  \App\Models\Variant  $productVariant
     * @return \Illuminate\Http\Response
     */
    public function removeVariant(Variant $productVariant)
    {
        try
        {
            $delete_id = $productVariant->id;
            $product_id = $productVariant->product->id;

            // delete all variant options
            $productVariant->options()->detach();

            // delete model
            $productVariant->delete();
            
            echo 1;
        }
        catch(Exception $e)
        {
            echo 0;
        }
    }

    /**
     * Toggles the is_featured column of the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function toggleFeatured(Product $product)
    {
        $product->is_featured = !$product->is_featured;
        $product->save();

        return redirect()->route('products.index')->with('status', config("app.success_message"));
    }

    private function saveProductMedia($request, $product_id)
    {
        if($request->hasfile('image_url'))
        {
            foreach($request->file('image_url') as $key => $image)
            {
                // Get Filename
                $filenameWithExt = $image->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $filename = $filename . '_'.  time() . '.' . $image->extension(); 

                // set image url as path and filename
                $image_url = str_replace("public/", "storage/", config("app.products_media_dir")) . '/' . $filename;

                // save file to path
                $image->storePubliclyAs(config("app.products_media_dir"), $filename);

                // generate thumbnails

                // save media
                $product_media = ProductMedia::create([
                    'product_id' => $product_id,
                    'media_type' => "image",
                    'media_url' => $image_url,
                    // 'is_main_image' => $key == 0 ? 1 : 0, // set first image as main image
                    'created_by' => Auth::user()->id,
                ]);
            }

            // set main_image in product media, first image always main image, hence
            $this->setMainImage($product_id);
        }
    }

    private function setMainImage($product_id) {
        // set main_images to 0
        ProductMedia::where('product_id', $product_id)
            ->update(['is_main_image' => 0]);
        
        // set first image to main_image
        $first_media = ProductMedia::where('product_id', $product_id)->where('is_main_image', 0)->first();
        if($first_media) {
            $first_media->is_main_image = 1;
            $first_media->save();
        }
        return true;
    }

    private function deleteImage(?string $url) {
        if($url) {
            Storage::disk('public')->delete(str_replace("storage/", "", $url));
        }
    }
}
