<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        // copy default images if it doesn't exist
        $system_default_img = public_path() . '/assets/images/' . config("app.default_image");
        $categories_public_url = str_replace("public/", "", config("app.categories_media_dir") . '/' . config("app.default_image"));
        $products_public_url = str_replace("public/", "", config("app.products_media_dir") . '/' . config("app.default_image"));
        $brands_public_url = str_replace("public/", "", config("app.brands_media_dir") . '/' . config("app.default_image"));
        

        if(file_exists($system_default_img) && !Storage::disk('public')->exists($categories_public_url) ) {
            // copy image to public path
            \File::copy($system_default_img, public_path() . "/storage/" . $categories_public_url);
        }

        if(file_exists($system_default_img) && !Storage::disk('public')->exists($products_public_url) ) {
            // copy image to public path
            \File::copy($system_default_img, public_path() . "/storage/" . $products_public_url);
        }

        if(file_exists($system_default_img) && !Storage::disk('public')->exists($brands_public_url) ) {
            // copy image to public path
            \File::copy($system_default_img, public_path() . "/storage/" . $brands_public_url);
        }
    }
}
