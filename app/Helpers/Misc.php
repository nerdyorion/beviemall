<?php

namespace App\Helpers;

// Custom Miscellaneous Methods

class Misc
{
    const IMG_SMALL_SIZE = 235;
    const IMG_SMALL_HEIGHT = 235;

    const IMG_MEDIUM_WIDTH = 400;
    const IMG_MEDIUM_HEIGHT = 400;

    const IMG_LARGE_WIDTH = 800;
    const IMG_LARGE_HEIGHT = 800;

    public static function ImageURL(?string $url) : string
    {
        if(!$url) return asset(str_replace("public/", "storage/", config("app.categories_media_dir")) . '/' . config("app.default_image"));
        return asset($url);
    }
}