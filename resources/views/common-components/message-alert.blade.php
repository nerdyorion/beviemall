@if ($errors->any())
    <div class="alert alert-border alert-border-danger alert-dismissible fade show" role="alert">
        <i class="uil uil-exclamation-octagon font-size-16 text-danger me-2"></i>
        @foreach ($errors->all() as $error)
            {{ $error }}<br />
        @endforeach
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif
@if (session('status'))
    <div class="alert alert-border alert-border-success alert-dismissible fade show" role="alert">
        <i class="uil uil-check font-size-16 text-success me-2"></i>
            {{ session('status') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
@endif