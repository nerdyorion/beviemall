@extends('layouts.master')
@section('title')
    @lang('translation.Products')
@endsection
@section('css')
    <!-- Lightbox css -->
    <link href="{{ URL::asset('/assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @component('common-components.breadcrumb')
        @slot('pagetitle') Dashboard @endslot
        @slot('title') @lang('translation.Products') @endslot
    @endcomponent

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <a href="{{ route('products.create') }}"  class="btn btn-success waves-effect waves-light">
                                    <i class="mdi mdi-plus me-2"></i> Add New
                                </a>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-inline float-md-end mb-3">
                                <div class="search-box ms-2">
                                    <div class="position-relative">
                                        <input type="text" class="form-control rounded bg-light border-0"
                                            placeholder="Search...">
                                        <i class="mdi mdi-magnify search-icon"></i>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                    <!-- end row -->
                    <div class="table-responsive mb-4">
                        <table class="table table-centered table-nowrap mb-0">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 50px;">
                                        <div class="form-check font-size-16">
                                            <input type="checkbox" class="form-check-input" id="contacusercheck">
                                            <label class="form-check-label" for="contacusercheck"></label>
                                        </div>
                                    </th>
                                    <th scope="col">Name</th>
                                    <th scope="col">SKU</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Active?</th>
                                    <th scope="col">Featured</th>
                                    <th scope="col" style="width: 200px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                <tr>
                                    <th scope="row">
                                        <div class="form-check font-size-16">
                                            <input type="checkbox" class="form-check-input" id="contacusercheck1">
                                            <label class="form-check-label" for="contacusercheck1"></label>
                                        </div>
                                    </th>
                                    <td>
                                        @if(count($item->media))
                                        <a class="image-popup-vertical-fit" href="{{ Misc::ImageURL($item->media->first()->media_url) }}" title="{{ $item->name }}">
                                        <img src="{{ Misc::ImageURL($item->media->first()->media_url) }}" alt=""
                                            class="avatar-xs rounded-circle me-2"></a>
                                        @endif
                                        <a href="#" class="text-body">{{ $item->name }}</a>
                                    </td>
                                    <td>{{ $item->sku }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>N{{ $item->price }}</td>
                                    <td>
                                        <div class="badge bg-soft-{{ $item->is_active ? 'success' : 'danger' }} font-size-12">{{ $item->is_active ? 'YES' : 'NO' }}</div>
                                    </td>
                                    <td>
                                        <a title="{{ $item->is_featured ? 'Remove' : 'Set' }} Featured Product?" href="{{ route('products.toggleFeatured', $item->id) }}" class="px-2 text-{{ $item->is_featured ? 'success' : 'danger' }}" 
                                            onclick="event.preventDefault(); if(confirm('Are you you want to do this?')) 
                                            document.getElementById('toggle-featured-{{ $item->id }}').submit();">

                                            <i class="uil uil-{{ $item->is_featured ? 'bolt' : 'bolt-slash' }} font-size-18"></i>
                                        </a>
                                        <form id="toggle-featured-{{ $item->id }}" action="{{ route('products.toggleFeatured', $item->id) }}" method="POST">
                                            @csrf
                                        </form>
                                    </td>
                                    <td>
                                        <ul class="list-inline mb-0">
                                            <li class="list-inline-item">
                                                <a href="{{ route('products.edit', ['product' => $item]) }}" class="px-2 text-primary"><i
                                                        class="uil uil-pen font-size-18"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="{{ route('products.create', ['clone' => $item]) }}" class="px-2 text-primary"><i
                                                        class="uil uil-copy font-size-18"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="{{ route('products.destroy', $item->id) }}" class="px-2 text-danger" onclick="event.preventDefault();
                                                if(confirm('Are you sure you want to do this?')) document.getElementById('delete-form-{{ $item->id }}').submit();">
                                                    <i class="uil uil-trash-alt font-size-18"></i>
                                                </a>
                                                <form id="delete-form-{{ $item->id }}" action="{{ route('products.destroy', $item->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </li>
                                            <!-- <li class="list-inline-item dropdown">
                                                <a class="text-muted dropdown-toggle font-size-18 px-2" href="#"
                                                    role="button" data-bs-toggle="dropdown" aria-haspopup="true">
                                                    <i class="uil uil-ellipsis-v"></i>
                                                </a>

                                                <div class="dropdown-menu dropdown-menu-end">
                                                    <a class="dropdown-item" href="{{ route('products.create', $item->id) }}" onclick="event.preventDefault();
                                                    if(confirm('Are you sure you want to do this?')) document.getElementById('clone-form-{{ $item->id }}').submit();">Clone</a>

                                                    <form id="clone-form-{{ $item->id }}" action="{{ route('products.create', $item->id) }}" method="POST">
                                                        @csrf
                                                    </form>
                                                </div>
                                            </li> -->
                                        </ul>
                                    </td>
                                </tr>
                                @endforeach

                                <!-- <tr>
                                    <th scope="row">
                                        <div class="form-check font-size-16">
                                            <input type="checkbox" class="form-check-input" id="contacusercheck1">
                                            <label class="form-check-label" for="contacusercheck1"></label>
                                        </div>
                                    </th>
                                    <td>
                                        <a class="image-popup-vertical-fit" href="{{ URL::asset('/assets/images/users/avatar-2.jpg') }}" title="Nike">
                                        <img src="{{ URL::asset('/assets/images/users/avatar-2.jpg') }}" alt=""
                                            class="avatar-xs rounded-circle me-2"></a>
                                        <a href="#" class="text-body">Nike</a>
                                    </td>
                                    <td>Full Stack Developer</td>
                                    <td>
                                        <div class="badge bg-soft-danger font-size-12">NO</div>
                                    </td>
                                    <td>
                                        <ul class="list-inline mb-0">
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0);" class="px-2 text-primary"><i
                                                        class="uil uil-pen font-size-18"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0);" class="px-2 text-danger"><i
                                                        class="uil uil-trash-alt font-size-18"></i></a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr> -->

                            </tbody>
                        </table>
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-6">
                            <div>
                                <p class="mb-sm-0">Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of {{ $data->total() }} entries</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-sm-end">
                                <!-- <ul class="pagination mb-sm-0">
                                    <li class="page-item disabled">
                                        <a href="#" class="page-link"><i class="mdi mdi-chevron-left"></i></a>
                                    </li>
                                    <li class="page-item">
                                        <a href="#" class="page-link">1</a>
                                    </li>
                                    <li class="page-item active">
                                        <a href="#" class="page-link">2</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="#" class="page-link">3</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="#" class="page-link"><i class="mdi mdi-chevron-right"></i></a>
                                    </li>
                                </ul> -->
                                {{ $data->onEachSide(2)->links() }}
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- Magnific Popup-->
    <script src="{{ URL::asset('/assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <!-- lightbox init js-->
    <script src="{{ URL::asset('/assets/js/pages/lightbox.init.js') }}"></script>
@endsection
