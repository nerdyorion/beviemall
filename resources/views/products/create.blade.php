@extends('layouts.master')
@section('title')
    @lang('translation.Add_Product')
@endsection
@section('css')
    <!-- plugin css -->
    <link href="{{ URL::asset('/assets/libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ URL::asset('/assets/libs/spectrum-colorpicker/spectrum-colorpicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    @component('common-components.breadcrumb')
        @slot('pagetitle') Products @endslot
        @slot('title') Add Product @endslot
    @endcomponent

    <form class="needs-validation" method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data" novalidate id="productForm">
        @csrf
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Product Info</h4>
                        <!-- <p class="card-title-desc">Add Product.</p> -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ $cloned_product['name'] ?? old('name') }}" required />
                                    <!-- <div class="valid-feedback">Looks good!</div> -->
                                    <div class="invalid-feedback">
                                        Please provide a valid name.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="sku">SKU</label>
                                    <input type="text" class="form-control" name="sku" id="sku" placeholder="SKU" value="{{ $cloned_product['sku'] ?? old('sku') }}" required />
                                    <div class="invalid-feedback">
                                        Please provide a valid SKU.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="price">Price</label>
                                    <input type="number" step="0.01" class="form-control" name="price" id="price" min="0" value="{{ $cloned_product['price'] ?? old('price') }}" required />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="price_without_tax">Price without Tax</label>
                                    <input type="number" step="0.01" class="form-control" name="price_without_tax" id="price_without_tax" min="0" value="{{ $cloned_product['price_without_tax'] ?? old('price_without_tax') }}" required />
                                    <div class="invalid-feedback">
                                        Please provide a valid Price without Tax.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="brand_id">Brand</label>
                                    <select class="form-select" name="brand_id" id="brand_id">
                                        <option value="">-- Select --</option>
                                        @foreach ($data["brands"] as $brand_id => $brand_value)
                                            <option value="{{ $brand_id }}" {{ old('brand_id') == $brand_id || (($cloned_product['brand_id'] ?? '') == $brand_id) ? "selected" : '' }}>{{ $brand_value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="weight_kg">Weight (Kg)</label>
                                    <input type="number" step="0.01" class="form-control" name="weight_kg" id="weight_kg" min="0" value="{{ $cloned_product['weight_kg'] ?? old('weight_kg') }}" required />
                                    <div class="invalid-feedback">
                                        Please provide a valid Weight(Kg).
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label" for="weight_kg">Description</label>
                                    <div id="description"></div>
                                    <input type="hidden" name="description" id="description_val" value="{{ $cloned_product['description'] ?? old('description') }}" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6">
                                <h5 class="font-size-14 mb-3">Featured?</h5>
                                <input type="checkbox" name="is_featured" id="is_featured" value="1" switch="bool" {{ isset($cloned_product['is_featured']) ? ($cloned_product['is_featured'] == 1 ? 'checked' : '') : (old('is_featured') == 1 ? 'checked' : '') }} />
                                <label for="is_featured" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                            <div class="col-lg-6">
                                <h5 class="font-size-14 mb-3">Active?</h5>
                                <input type="checkbox" name="is_active" id="is_active" value="1" switch="bool" {{ isset($cloned_product['is_active'])  ? ($cloned_product['is_active'] ? 'checked' : '') : 'checked' }} />
                                <label for="is_active" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Product Category & Sub-Categories</h4>
                        <!-- <p class="card-title-desc">Add Product.</p> -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label" for="categories">Choose one or more categories</label>
                                    <ul class="list-unstyled">
                                    @foreach ($data["categories"] as $category)
                                        <li>
                                            <input type="checkbox" class="nested-chk" id="category-{{ $category->id }}" value="{{ $category->id }}" {{ in_array($category->id, (array)($cloned_product['categories'] ?? old('categories'))) ? 'checked' : '' }} />
                                            <label class="" for="category-{{ $category->id }}">{{ $category->name }}</label>
                                            @if ($category->children)
                                                @if (count($category->children))
                                                    <!-- Subcategory(ies) -->
                                                    @include('product-categories.subcategory-checkboxes',['subcategories' => $category->children, 'selected_categories' => $cloned_product['categories'] ?? old('categories'), 'indentation' => ['&nbsp;&nbsp;&nbsp;&nbsp;'], 'parentElementId' => "category-{{ $category->id }}"])
                                                @endif
                                            @endif
                                        </li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div>
                            <ul>
                            </ul>
                        </div>

                    </div>
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Product Images</h4>
                        <!-- <p class="card-title-desc">Add Product.</p> -->

                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label" for="">Add one or more images  (<b>NB: The first image is the main image</b>)</label>

                                    <div class="input-group hdtuto control-group lst increment" >
                                        <input type="file" name="image_url[]" class="myfrm form-control">
                                        <div class="input-group-btn"> 
                                            <button class="btn btn-success image_add_btn" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                                        </div>
                                    </div>

                                    <div class="clone hide">
                                        <div class="hdtuto control-group lst input-group" style="margin-top:10px">
                                            <input type="file" name="image_url[]" class="myfrm form-control">
                                            <div class="input-group-btn"> 
                                                <button class="btn btn-danger image_remove_btn" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>




                        
                    </div>
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Product Variant</h4>
                        <p class="card-title-desc">Add one or more variations</p>

                        @if(isset($cloned_product['variants']))
                            @foreach($cloned_product['variants'] as $variant)
                                <div class="row variant-block hide">

                                    <div class="col-md-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="variations">Option</label>

                                            <select class="select2 form-control select2-multiple variant_options" multiple="multiple" data-placeholder="Choose one or more ...">

                                                @foreach ($options as $optgroup => $options_vals)
                                                    <optgroup label="{{ $optgroup }}">
                                                        @foreach ($options_vals as $option)
                                                            <option value="{{ $option['id'] }}" {{ in_array($option['id'], (array)$variant['options']) ? 'selected' : '' }}>{{ $option->value }}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="mb-3">
                                            <label class="form-label">Selection</label>
                                            <select class="form-select variant_selection_type cloned">
                                                <option value="rectangle-list" {{ $variant['selection_type'] == 'rectangle-list' ? "selected" : '' }}>Rectangle List</option>
                                                <option value="swatch" {{ $variant['selection_type'] == 'swatch' ? "selected" : '' }}>Swatch</option>
                                                <option value="radio" {{ $variant['selection_type'] == 'radio' ? "selected" : '' }}>Radio</option>
                                                <option value="dropdown" {{ $variant['selection_type'] == 'dropdown' ? "selected" : '' }}>Dropdown</option>
                                            </select>

                                            <input type="text" class="form-control variant_color_hex colorpicker-showinput-intial" value="{{ $variant['color_hex'] }}" />
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="mb-3">
                                            <label class="form-label">Price</label>
                                            <input type="number" step="0.01" class="form-control variant_price" min="0" value="{{ $variant['price'] }}" />
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="input-group hdtuto control-group">
                                            <div class="form-check mb-3 mt-3">
                                                <input class="form-check-input variant_is_default" type="radio" name="variant_is_default" id="variant_is_default_1" {{ $variant['is_default'] == '1' ? "checked" : '' }} />
                                                <label class="form-check-label" for="variant_is_default_1">
                                                    Default?&nbsp;&nbsp;
                                                </label>
                                            </div>
                                            <div class="input-group-btn mt-3"> 
                                                <button class="btn btn-danger variant_remove_btn" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                        <div class="row variant-block">

                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label class="form-label" for="variations">Option</label>

                                    <select class="select2 form-control select2-multiple variant_options" multiple="multiple" data-placeholder="Choose one or more ...">

                                        @foreach ($options as $optgroup => $options_vals)
                                            <optgroup label="{{ $optgroup }}">
                                                @foreach ($options_vals as $option)
                                                    <option value="{{ $option['id'] }}">{{ $option->value }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label class="form-label">Selection</label>
                                    <select class="form-select variant_selection_type">
                                        <option value="rectangle-list">Rectangle List</option>
                                        <option value="swatch">Swatch</option>
                                        <option value="radio">Radio</option>
                                        <option value="dropdown">Dropdown</option>
                                    </select>

                                    <input type="text" class="form-control variant_color_hex colorpicker-showinput-intial" />
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="mb-3">
                                    <label class="form-label">Price</label>
                                    <input type="number" step="0.01" class="form-control variant_price" min="0" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="input-group hdtuto control-group">
                                    <div class="form-check mb-3 mt-3">
                                        <input class="form-check-input variant_is_default" type="radio" name="variant_is_default" id="variant_is_default_1" />
                                        <label class="form-check-label" for="variant_is_default_1">
                                            Default?&nbsp;&nbsp;
                                        </label>
                                    </div>
                                    <div class="input-group-btn mt-3"> 
                                        <button class="btn btn-success variant_add_btn" type="button"><i class="glyphicon glyphicon-plus"></i>Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- 
                        <div class="row variant-block hide">

                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label class="form-label" for="variations">Option</label>

                                    <select class="select2 form-control select2-multiple variant_options" multiple="multiple" data-placeholder="Choose one or more ...">

                                        @foreach ($options as $optgroup => $options_vals)
                                            <optgroup label="{{ $optgroup }}">
                                                @foreach ($options_vals as $option)
                                                    <option value="{{ $option['id'] }}">{{ $option->value }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="mb-3">
                                    <label class="form-label">Selection</label>
                                    <select class="form-select variant_selection_type">
                                        <option value="rectangle-list">Rectangle List</option>
                                        <option value="swatch">Swatch</option>
                                        <option value="radio">Radio</option>
                                        <option value="dropdown">Dropdown</option>
                                    </select>

                                    <input type="text" class="form-control variant_color_hex colorpicker-showinput-intial" />
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="mb-3">
                                    <label class="form-label">Price</label>
                                    <input type="number" step="0.01" class="form-control variant_price" min="0" />
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="input-group hdtuto control-group">
                                    <div class="form-check mb-3 mt-3">
                                        <input class="form-check-input variant_is_default" type="radio" name="variant_is_default" id="variant_is_default_1" />
                                        <label class="form-check-label" for="variant_is_default_1">
                                            Default?&nbsp;&nbsp;
                                        </label>
                                    </div>
                                    <div class="input-group-btn mt-3"> 
                                        <button class="btn btn-danger variant_remove_btn" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                         -->
                    </div>
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Product Inventory &amp; Shipping</h4>
                        <!-- <p class="card-title-desc">Add Product.</p> -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="quantity">Quantity</label>
                                    <input type="number" step="1" class="form-control" name="quantity" id="quantity" min="0" value="{{ $cloned_product['quantity'] ?? old('quantity') }}" required />
                                    <div class="invalid-feedback">
                                        Please provide a valid quantity
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="quantity_low_mark">Quantity Low Level</label>
                                    <input type="number" step="1" class="form-control" name="quantity_low_mark" id="quantity_low_mark" min="0" value="{{ $cloned_product['quantity_low_mark'] ?? old('quantity_low_mark') }}" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="fixed_shipping_fee">Fixed Shipping Fee</label>
                                    <input type="number" step="1" class="form-control" name="fixed_shipping_fee" id="fixed_shipping_fee" min="0" value="{{ $cloned_product['fixed_shipping_fee'] ?? old('fixed_shipping_fee') }}" />
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <h5 class="font-size-14 mb-3">Free Shipping?</h5>
                                <input type="checkbox" name="is_free_shipping" id="is_free_shipping" value="1" switch="bool" {{ isset($cloned_product['is_free_shipping']) ? ($cloned_product['is_free_shipping'] == 1 ? 'checked' : '') : (old('is_free_shipping') == 1 ? 'checked' : '') }}  />
                                <label for="is_free_shipping" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div>

        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Product SEO</h4>
                        <!-- <p class="card-title-desc">Add Product.</p> -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label" for="page_meta_title">Page Meta Title</label>
                                        <input type="text" class="form-control" name="page_meta_title" id="page_meta_title" placeholder="Page Meta Title" value="{{ $cloned_product['page_meta_title'] ?? old('page_meta_title') }}" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="mb-3">
                                        <label class="form-label" for="page_meta_description">Page Meta Description</label>
                                        <textarea class="form-control" name="page_meta_description" id="page_meta_description" rows="3">{{ $cloned_product['page_meta_description'] ?? old('page_meta_description') }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
                <!-- end card -->
            </div> <!-- end col -->
        </div>
        <!-- end row -->
    
    </form>

@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/select2/select2.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/spectrum-colorpicker/spectrum-colorpicker.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/ckeditor/ckeditor.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
    <script>
        let editor_;

        ClassicEditor
        .create(document.querySelector('#description'))
        .then( editor => {
            editor_ = editor;
            editor.setData("{!! $cloned_product['description'] ?? old('description') !!}");
        })
        .catch(error => {
            console.error(error);
        });


        $('input.nested-chk[type="checkbox"]').change(function(e) {
            var checked = $(this).prop("checked"),
            container = $(this).parent(),
            siblings = container.siblings();

            container.find('input.nested-chk[type="checkbox"]').prop({
                indeterminate: false,
                checked: checked
            });

            function checkSiblings(el) {

                var parent = el.parent().parent(),
                all = true;

                el.siblings().each(function() {
                    let returnValue = all = ($(this).children('input.nested-chk[type="checkbox"]').prop("checked") === checked);
                    return returnValue;
                });
            
                if (all && checked) {
                    parent.children('input.nested-chk[type="checkbox"]').prop({
                        indeterminate: false,
                        checked: checked
                    });

                    checkSiblings(parent);

                } else if (all && !checked) {
                    parent.children('input.nested-chk[type="checkbox"]').prop("checked", checked);
                    parent.children('input.nested-chk[type="checkbox"]').prop("indeterminate", (parent.find('input.nested-chk[type="checkbox"]:checked').length > 0));
                    checkSiblings(parent);
                } else {
                    el.parents("li").children('input.nested-chk[type="checkbox"]').prop({
                        indeterminate: true,
                        checked: false
                    });
                }
            }

            checkSiblings(container);
        });

        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict'

            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.querySelectorAll('.needs-validation')

            // Loop over them and prevent submission
            Array.prototype.slice.call(forms)
            .forEach(function (form) {
                form.addEventListener('submit', function (event) {
                    if(!form.checkValidity()) {
                        event.preventDefault()
                        event.stopPropagation()
                    }

                    form.classList.add('was-validated')
                }, false)
            })
        })();

        $('#productForm').submit(function() {
            // check if form validation passed
            let productForm = document.getElementById("productForm");
            formIsValid = productForm.checkValidity();

            if(!formIsValid) return false;

            const editorData = editor_.getData();

            let formRef = this;
            let formData;

            $("#description_val").val(editorData);

            $("input.nested-chk:checked").each(function() {
                /* Attach categories */
                let appendCategory = '<input type="hidden" name="categories[]" value="' + this.value + '" />';
                $("#description_val").after(appendCategory);
            });

            // get variant options
            $(".variant_options").each(function() {
                /* Attach variant options */
                // console.log($(this).val());

                // remove [ and ] from string
                let result = JSON.stringify($(this).val());
                result = result.slice(1,-1).replace(/"/gi, ""); // remove " from string
                // console.log(result);

                // console.log($(this).select2('data'));
                let appendVariantOptions = '<input type="hidden" name="variant_options[]" value="' + result + '" />';
                $("#description_val").after(appendVariantOptions);
            });

            // get variant selection types
            $(".variant_selection_type").each(function() {
                /* Attach variant selection types */
                // console.log($(this).val());
                let appendVariantSelectionType = '<input type="hidden" name="variant_selection_types[]" value="' + this.value + '" />';
                $("#description_val").after(appendVariantSelectionType);
            });

            // get variant color hexes
            $(".variant_color_hex").each(function() {
                /* Attach variant color hexes */
                // console.log($(this).val());
                let appendVariantColorHex = '<input type="hidden" name="variant_color_hexes[]" value="' + this.value + '" />';
                $("#description_val").after(appendVariantColorHex);
            });

            // get variant prices
            $(".variant_price").each(function() {
                /* Attach variant prices */
                // console.log($(this).val());
                let appendVariantPrice = '<input type="hidden" name="variant_prices[]" value="' + this.value + '" />';
                $("#description_val").after(appendVariantPrice);
            });

            // get variant default
            $(".variant_is_default").each(function() {
                /* Attach variant default */
                let isChecked = $(this).is(':checked');
                let str_val = isChecked ? '1' : '0';

                // console.log("default", isChecked);
                let appendVariantIsDefault = '<input type="hidden" name="variant_is_defaults[]" value="' + str_val + '" />';
                $("#description_val").after(appendVariantIsDefault);
            });
            
            // return false;
        });

        $(document).ready(function() {
            // add image
            $(".image_add_btn").click(function(){
                let lastImageHTMLBlock = $(".clone").html();

                if(lastImageHTMLBlock.trim() == '') {
                    lastImageHTMLBlock = '<div class="clone hide"><div class="hdtuto control-group lst input-group" style="margin-top:10px"><input type="file" name="image_url[]" class="myfrm form-control"><div class="input-group-btn"><button class="btn btn-danger image_remove_btn" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button></div></div></div>';
                }
                $(".increment").after(lastImageHTMLBlock);
            });

            // remove image
            $("body").on("click",".image_remove_btn",function(){ 
                $(this).parents(".hdtuto").remove();
            });

            // hide swatches on load
            // $(".variant_selection_type").next().hide();

            function toggleSwatchInput() {
                $(".variant_selection_type").next().hide();
                
                // 
                $("body").on("change",".variant_selection_type",function(){
                    let next_color_ele = $($(this).next().find(".variant_color_hex")[0]);

                    if($(this).val() == 'swatch') {
                        // show swatch input
                        // console.log("swatch", next_color_ele.val());
                        $(this).next().show();
                    } else {
                        // hide swatch input
                        // console.log("not swatch");
                        next_color_ele.val('');
                        $(this).next().hide();
                    }
                });
            }

            // add variant
            $(".variant_add_btn").click(function(){
                // let lastHTMLBlock = $(".variant-block").html();
                let new_id_no = Math.random().toString(16).slice(2);

                let lastHTMLBlock = '<div class="row variant-block hide"><div class="col-md-3"><div class="mb-3"> <label class="form-label" for="variations">Option</label><select id="select_' + new_id_no + '" class="select2 form-control select2-multiple variant_options" multiple="multiple" data-placeholder="Choose one or more ...">@foreach ($options as $optgroup => $options_vals)<optgroup label="{{ $optgroup }}"> @foreach ($options_vals as $option)<option value="{{ $option['id'] }}">{{ $option->value }}</option> @endforeach</optgroup> @endforeach </select></div></div><div class="col-md-3"><div class="mb-3"> <label class="form-label">Selection</label> <select id="variant_selection_type_' + new_id_no + '" class="form-select variant_selection_type"><option value="rectangle-list">Rectangle List</option><option value="swatch">Swatch</option><option value="radio">Radio</option><option value="dropdown">Dropdown</option> </select><input type="text" id="color_' + new_id_no + '" class="form-control variant_color_hex colorpicker-showinput-intial" /></div></div><div class="col-md-2"><div class="mb-3"> <label class="form-label">Price</label> <input type="number" step="0.01" class="form-control variant_price" min="0" /></div></div><div class="col-md-4"><div class="input-group hdtuto control-group" ><div class="form-check mb-3 mt-3"> <input class="form-check-input variant_is_default" type="radio" name="variant_is_default" id="variant_is_default_' + new_id_no + '" /> <label class="form-check-label" for="variant_is_default_' + new_id_no + '"> Default?&nbsp;&nbsp; </label></div><div class="input-group-btn mt-3"> <button class="btn btn-danger variant_remove_btn" type="button"><i class="glyphicon glyphicon-remove"></i> Remove</button></div></div></div></div>';

                $(".variant-block").last().after(lastHTMLBlock);

                $("#select_" + new_id_no).select2();

                // color picker
                $("#color_" + new_id_no).spectrum({
                    showInitial: true,
                    showInput: true
                }); //Bootstrap-TouchSpin

                // hide swatch color picker
                $("#variant_selection_type_" + new_id_no).next().hide();
            });

            // remove variant
            $("body").on("click",".variant_remove_btn",function(){ 
                $(this).parents(".variant-block").remove();
            });

            // Select2
            $(".select2").select2();

            // color picker
            $(".colorpicker-showinput-intial").spectrum({
                showInitial: true,
                showInput: true
            }); //Bootstrap-TouchSpin

            // toggle swatch inputs
            toggleSwatchInput();

            $(".variant_selection_type.cloned").each(function() {
                // console.log($(this).val());
                let next_color_ele = $($(this).next().find(".variant_color_hex")[0]);

                if($(this).val() == 'swatch') {
                    // show swatch input
                    // console.log("swatch", next_color_ele.val());
                    $(this).next().show();
                } else {
                    // hide swatch input
                    // console.log("not swatch");
                    next_color_ele.val('');
                    $(this).next().hide();
                }

            });
        });

    </script>
@endsection
