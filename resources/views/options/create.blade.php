@extends('layouts.master')
@section('title')
    @lang('translation.Add_Option')
@endsection

@section('content')
    @component('common-components.breadcrumb')
        @slot('pagetitle') Options @endslot
        @slot('title') Add Option @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title">Products</h4> -->
                    <!-- <p class="card-title-desc">Add Product.</p> -->
                    <form class="needs-validation" method="POST" action="{{ route('options.store') }}" enctype="multipart/form-data" novalidate>
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label" for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ old('name') }}" required />
                                    <!-- <div class="valid-feedback">Looks good!</div> -->
                                    <div class="invalid-feedback">
                                        Please provide a valid name.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label" for="value">Value</label>
                                    <input type="text" class="form-control" name="value" id="value" placeholder="Value" value="{{ old('value') }}" required />
                                    <!-- <div class="valid-feedback">Looks good!</div> -->
                                    <div class="invalid-feedback">
                                        Please provide a valid value.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <h5 class="font-size-14 mb-3">Active?</h5>
                                <input type="checkbox" name="is_active" id="is_active" value="1" switch="bool" checked />
                                <label for="is_active" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                        </div>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </form>
                </div>
            </div>
            <!-- end card -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
@endsection
