@if (count($category->children))
    <ul class="list-unstyled">
        @foreach ($subcategories as $child)
            <li>
    
                @php
                    $indent = implode('', $indentation)
                @endphp

                {!! $indent !!}<input type="checkbox" class="nested-chk" id="{{ $parentElementId }}-{{ $child->id }}" value="{{ $child->id }}" {{ in_array($child->id, (array)$selected_categories) ? 'checked' : '' }} />
                <label class="" for="{{ $parentElementId }}-{{ $child->id }}">{{ $child->name }}</label>

                @if(count($child->children))
                    @php
                        $indentation[] = $indentation[0] ?? '';
                        $parentElementId .= '-' . $child->id;
                    @endphp

                    @include('product-categories.subcategory-checkboxes',['subcategories' => $child->children, 'selected_categories' => $selected_categories, 'indentation' => $indentation, 'parentElementId' => $parentElementId])

                    @php
                        // delete last indent element
                        array_pop($indentation);
                    @endphp
                @endif
            </li>
        @endforeach
    </ul>
@endif