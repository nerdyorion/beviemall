@extends('layouts.master')
@section('title')
    @lang('translation.Categories')
@endsection
@section('css')
    <!-- Lightbox css -->
    <link href="{{ URL::asset('/assets/libs/magnific-popup/magnific-popup.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @component('common-components.breadcrumb')
        @slot('pagetitle') Dashboard @endslot
        @slot('title') @lang('translation.Categories') @endslot
    @endcomponent

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="row mb-2">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <a href="{{ route('product-categories.create') }}"  class="btn btn-success waves-effect waves-light">
                                    <i class="mdi mdi-plus me-2"></i> Add New
                                </a>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-inline float-md-end mb-3">
                                <div class="search-box ms-2">
                                    <div class="position-relative">
                                        <input type="text" class="form-control rounded bg-light border-0"
                                            placeholder="Search...">
                                        <i class="mdi mdi-magnify search-icon"></i>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                    <!-- end row -->
                    <div class="table-responsive mb-4">
                        <table class="table table-centered table-nowrap mb-0">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 50px;">
                                        <div class="form-check font-size-16">
                                            <input type="checkbox" class="form-check-input" id="contacusercheck">
                                            <label class="form-check-label" for="contacusercheck"></label>
                                        </div>
                                    </th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Parent Category</th>
                                    <th scope="col">Active?</th>
                                    <th scope="col" style="width: 200px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                <tr>
                                    <th scope="row">
                                        <div class="form-check font-size-16">
                                            <input type="checkbox" class="form-check-input" id="contacusercheck1">
                                            <label class="form-check-label" for="contacusercheck1"></label>
                                        </div>
                                    </th>
                                    <td>
                                        <a class="image-popup-vertical-fit" href="{{ Misc::ImageURL($item->image_url) }}" title="{{ $item->name }}">
                                        <img src="{{ Misc::ImageURL($item->image_url) }}" alt=""
                                            class="avatar-xs rounded-circle me-2"></a>
                                        <a href="#" class="text-body">{{ $item->name }}</a>
                                    </td>
                                    <td>{{ $item->parent->name ?? '-' }}</td>
                                    <td>
                                        <div class="badge bg-soft-{{ $item->is_active ? 'success' : 'danger' }} font-size-12">{{ $item->is_active ? 'YES' : 'NO' }}</div>
                                    </td>
                                    <td>
                                        <ul class="list-inline mb-0">
                                            <li class="list-inline-item">
                                                <a href="{{ route('product-categories.edit', ['product_category' => $item]) }}" class="px-2 text-primary"><i
                                                        class="uil uil-pen font-size-18"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="{{ route('product-categories.destroy', $item->id) }}" class="px-2 text-danger" onclick="event.preventDefault();
                                                if(confirm('Are you you want to do this?')) document.getElementById('delete-form-{{ $item->id }}').submit();">
                                                    <i class="uil uil-trash-alt font-size-18"></i>
                                                </a>
                                                <form id="delete-form-{{ $item->id }}" action="{{ route('product-categories.destroy', $item->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                @if (count($item->children))
                                    <!-- Subcategory(ies) -->
                                    @include('product-categories.subcategory-list',['subcategories' => $item->children])
                                @endif

                                @endforeach

                                <!-- <tr>
                                    <th scope="row">
                                        <div class="form-check font-size-16">
                                            <input type="checkbox" class="form-check-input" id="contacusercheck1">
                                            <label class="form-check-label" for="contacusercheck1"></label>
                                        </div>
                                    </th>
                                    <td>
                                        <a class="image-popup-vertical-fit" href="{{ URL::asset('/assets/images/users/avatar-2.jpg') }}" title="Nike">
                                        <img src="{{ URL::asset('/assets/images/users/avatar-2.jpg') }}" alt=""
                                            class="avatar-xs rounded-circle me-2"></a>
                                        <a href="#" class="text-body">Nike</a>
                                    </td>
                                    <td>Full Stack Developer</td>
                                    <td>
                                        <div class="badge bg-soft-danger font-size-12">NO</div>
                                    </td>
                                    <td>
                                        <ul class="list-inline mb-0">
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0);" class="px-2 text-primary"><i
                                                        class="uil uil-pen font-size-18"></i></a>
                                            </li>
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0);" class="px-2 text-danger"><i
                                                        class="uil uil-trash-alt font-size-18"></i></a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr> -->

                            </tbody>
                        </table>
                    </div>
                    <div class="row mt-4">
                        <div class="col-sm-6">
                            <div>
                                <p class="mb-sm-0">Showing {{ $data->firstItem() }} to {{ $data->lastItem() }} of {{ $data->total() }} entries</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="float-sm-end">
                                <!-- <ul class="pagination mb-sm-0">
                                    <li class="page-item disabled">
                                        <a href="#" class="page-link"><i class="mdi mdi-chevron-left"></i></a>
                                    </li>
                                    <li class="page-item">
                                        <a href="#" class="page-link">1</a>
                                    </li>
                                    <li class="page-item active">
                                        <a href="#" class="page-link">2</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="#" class="page-link">3</a>
                                    </li>
                                    <li class="page-item">
                                        <a href="#" class="page-link"><i class="mdi mdi-chevron-right"></i></a>
                                    </li>
                                </ul> -->
                                {{ $data->onEachSide(2)->links() }}
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- Magnific Popup-->
    <script src="{{ URL::asset('/assets/libs/magnific-popup/magnific-popup.min.js') }}"></script>
    <!-- lightbox init js-->
    <script src="{{ URL::asset('/assets/js/pages/lightbox.init.js') }}"></script>
@endsection
