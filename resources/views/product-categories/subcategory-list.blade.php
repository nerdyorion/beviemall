@foreach ($subcategories as $child)
    <tr>
        <th scope="row">
            <div class="form-check font-size-16">
                <input type="checkbox" class="form-check-input" id="contacusercheck1">
                <label class="form-check-label" for="contacusercheck1"></label>
            </div>
        </th>
        <td>
            <a class="image-popup-vertical-fit" href="{{ Misc::ImageURL($child->image_url) }}" title="{{ $child->name }}">
            <img src="{{ Misc::ImageURL($child->image_url) }}" alt=""
                class="avatar-xs rounded-circle me-2"></a>
            <a href="#" class="text-body">{{ $child->name }}</a>
        </td>
        <td>{{ $child->parent->name ?? '-' }}</td>
        <td>
            <div class="badge bg-soft-{{ $child->is_active ? 'success' : 'danger' }} font-size-12">{{ $child->is_active ? 'YES' : 'NO' }}</div>
        </td>
        <td>
            <ul class="list-inline mb-0">
                <li class="list-inline-item">
                    <a href="{{ route('product-categories.edit', ['product_category' => $child]) }}" class="px-2 text-primary"><i
                            class="uil uil-pen font-size-18"></i></a>
                </li>
                <li class="list-inline-item">
                    <a href="{{ route('product-categories.destroy', $child->id) }}" class="px-2 text-danger" onclick="event.preventDefault();
                    if(confirm('Are you you want to do this?')) document.getElementById('delete-form-{{ $child->id }}').submit();">
                        <i class="uil uil-trash-alt font-size-18"></i>
                    </a>
                    <form id="delete-form-{{ $child->id }}" action="{{ route('product-categories.destroy', $child->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                    </form>
                </li>
            </ul>
        </td>
    </tr>
    @if(count($child->children))
        @include('product-categories.subcategory-list',['subcategories' => $child->children])
    @endif
@endforeach