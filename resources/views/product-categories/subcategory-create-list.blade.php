@foreach ($subcategories as $child)
    
    @php
        $indent = implode('', $indentation)
    @endphp

    <option value="{{ $child->id }}" {{ $child->id === $product_category_id ? 'selected' : '' }} >{!! $indent !!}{{ $child->name }}</option>

    @if(count($child->children))
        @php
            $indentation[] = $indentation[0] ?? '';
        @endphp

        @include('product-categories.subcategory-create-list',['subcategories' => $child->children, 'product_category_id' => $product_category_id, 'indentation' => $indentation])

        @php
            // delete last indent element
            array_pop($indentation);
        @endphp
    @endif
@endforeach