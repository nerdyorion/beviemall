@extends('layouts.master')
@section('title')
    @lang('translation.Update_Category')
@endsection

@section('content')
    @component('common-components.breadcrumb')
        @slot('pagetitle') Categories @endslot
        @slot('title') Update Category @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title">Products</h4> -->
                    <!-- <p class="card-title-desc">Add Product.</p> -->
                    <form class="needs-validation" method="POST" action="{{ route('product-categories.update', ['product_category' => $productCategory]) }}" enctype="multipart/form-data" novalidate>
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label" for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ $productCategory->name }}" required />
                                    <!-- <div class="valid-feedback">Looks good!</div> -->
                                    <div class="invalid-feedback">
                                        Please provide a valid name.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="parent_id">Parent</label>
                                    <select class="form-select" name="parent_id" id="parent_id">
                                        <option value="">-- Select --</option>
                                        @foreach ($data["categories"] as $category)
                                            <option value="{{ $category->id }}" {{ $productCategory->parent_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>

                                            @if (count($category->children))
                                                <!-- Subcategory(ies) -->
                                                @include('product-categories.subcategory-create-list',['subcategories' => $category->children, 'product_category_id' => $productCategory->parent_id, 'indentation' => ['&nbsp;&nbsp;&nbsp;&nbsp;']])
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <h5 class="font-size-14 mb-3">Active?</h5>
                                <input type="checkbox" name="is_active" id="is_active" value="1" switch="bool" {{ $productCategory->is_active ? 'checked' : '' }} />
                                <label for="is_active" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="image_url">Image</label>
                                    <input class="form-control" type="file" name="image_url" id="image_url" />
                                    <div class="form-text">{{ basename($productCategory->image_url) }}</div>
                                    <div class="invalid-feedback">
                                        Please provide a valid file.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-lg-12">
                                <div class="mb-3">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="invalidCheck" required>
                                        <label class="form-check-label" for="invalidCheck">Agree to terms and
                                            conditions</label>
                                        <div class="invalid-feedback">
                                            You must agree before submitting.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <button class="btn btn-primary" type="submit">Submit</button>
                        <button type="reset" class="btn btn-secondary waves-effect">Cancel</button>
                    </form>
                </div>
            </div>
            <!-- end card -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
@endsection
