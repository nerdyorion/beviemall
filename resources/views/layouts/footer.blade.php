<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-6">
                <script>document.write(new Date().getFullYear())</script> © {{ config('app.name', 'Laravel') }}.
            </div>
            <div class="col-sm-6">
                <div class="text-sm-end d-none d-sm-block">
                    Crafted with <i class="mdi mdi-heart text-danger"></i> by <a href="http://paperlessystems.com.ng/" target="_blank" class="text-reset">Paperless Systems</a>
                </div>
            </div>
        </div>
    </div>
</footer>