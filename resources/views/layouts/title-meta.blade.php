<meta charset="utf-8" />
<title>@yield('title') | {{ config('app.name', 'Laravel') }}</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta content="{{ config('app.name', 'Laravel') }}" name="description" />
<meta content="Paperless Systems" name="author" />
<!-- App favicon -->
<link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.ico')}}">