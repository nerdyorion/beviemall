@extends('layouts.master')
@section('title')
    @lang('translation.Update_Brand')
@endsection

@section('content')
    @component('common-components.breadcrumb')
        @slot('pagetitle') Brands @endslot
        @slot('title') Update Brand @endslot
    @endcomponent

    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <!-- <h4 class="card-title">Products</h4> -->
                    <!-- <p class="card-title-desc">Add Product.</p> -->
                    <form class="needs-validation" method="POST" enctype="multipart/form-data" action="{{ route('brands.update', ['brand' => $brand]) }}"  novalidate>
                        @method('PUT')
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label" for="name">Name</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="{{ $brand->name }}" required />
                                    <!-- <div class="valid-feedback">Looks good!</div> -->
                                    <div class="invalid-feedback">
                                        Please provide a valid name.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="mb-3">
                                    <label class="form-label" for="short_name"> Short Name</label>
                                    <input type="text" class="form-control" name="short_name" id="short_name" placeholder="Short Name" value="{{ $brand->short_name }}" />
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-4">
                                <div class="mb-3">
                                    <label class="form-label" for="validationCustom03">City</label>
                                    <input type="text" class="form-control" id="description" placeholder="City"
                                        required>
                                    <div class="invalid-feedback">
                                        Please provide a valid city.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-3">
                                    <label class="form-label" for="validationCustom04">State</label>
                                    <input type="text" class="form-control" id="validationCustom04" placeholder="State"
                                        required>
                                    <div class="invalid-feedback">
                                        Please provide a valid state.
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mb-3">
                                    <label class="form-label" for="validationCustom05">Zip</label>
                                    <input type="text" class="form-control" id="validationCustom05" placeholder="Zip"
                                        required>
                                    <div class="invalid-feedback">
                                        Please provide a valid zip.
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-lg-6">
                                <h5 class="font-size-14 mb-3">Active?</h5>
                                <input type="checkbox" name="is_active" id="is_active" switch="bool" value="1" {{ $brand->is_active ? 'checked' : '' }} />
                                <label for="is_active" data-on-label="Yes" data-off-label="No"></label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="mb-3">
                                    <label class="form-label" for="image_url">Image</label>
                                    <input class="form-control" type="file" name="image_url" id="image_url" />
                                    <div class="form-text">{{ basename($brand->image_url) }}</div>
                                    <div class="invalid-feedback">
                                        Please provide a valid file.
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="row">
                            <div class="col-lg-12">
                                <div class="mb-3">
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" id="invalidCheck" required>
                                        <label class="form-check-label" for="invalidCheck">Agree to terms and
                                            conditions</label>
                                        <div class="invalid-feedback">
                                            You must agree before submitting.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <button class="btn btn-primary" type="submit">Submit</button>
                        <button type="reset" class="btn btn-secondary waves-effect">Cancel</button>
                    </form>
                </div>
            </div>
            <!-- end card -->
        </div> <!-- end col -->
    </div>
    <!-- end row -->

@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/parsleyjs/parsleyjs.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/form-validation.init.js') }}"></script>
@endsection
