<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## BevieMall

BevieMall is a luxurious fashion brand.

## App Setup

Please clone the repo and fill your DB credentials in the .env file

Switch to your app folder on the terminal and run these commands.
1. npm install
2. composer install
3. php artisan key:generate
4. npm run dev (or npm run production for live environment)
5. php artisan optimize
6. php artisan migrate
7. php artisan db:seed
8. php artisan storage:link

**PS**: If you need to change the theme skin, please check the Minible Theme's documentation from the purchased zip.