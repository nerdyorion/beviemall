<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_history', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained()->onUpdate('restrict')->onDelete('restrict');
            $table->decimal('former_price', 10, 2)->nullable();
            $table->decimal('new_price', 10, 2)->nullable();

            $table->foreignId('created_by');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('restrict')->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('price_history', function (Blueprint $table){
            // $table->dropForeign(['product_id']);
            $table->dropForeign('price_history_product_id_foreign');
            $table->dropForeign('price_history_created_by_foreign');
        });
        Schema::dropIfExists('price_history');
    }
}
