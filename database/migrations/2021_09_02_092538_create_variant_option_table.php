<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variant_option', function (Blueprint $table) {
            // $table->id();
            $table->foreignId('variant_id')->constrained()->onUpdate('restrict')->onDelete('restrict');
            $table->foreignId('option_id')->constrained()->onUpdate('restrict')->onDelete('restrict');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variant_option', function (Blueprint $table){
            $table->dropForeign('variant_option_product_id_foreign');
            $table->dropForeign('variant_option_option_id_foreign');
        });

        Schema::dropIfExists('variant_option');
    }
}
