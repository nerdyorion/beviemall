<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255);
            $table->string('short_name', 50)->nullable();
            $table->string('image_url', 2000)->nullable();
            $table->boolean('is_active')->default(true);

            $table->foreignId('created_by');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('restrict')->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brands', function (Blueprint $table){
            $table->dropForeign('brands_created_by_foreign');
        });

        Schema::dropIfExists('brands');
    }
}
