<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_product_categories', function (Blueprint $table) {
            // $table->id();
            $table->foreignId('product_id')->constrained()->onUpdate('restrict')->onDelete('restrict');
            $table->foreignId('product_category_id');
            $table->foreign('product_category_id')->references('id')->on('product_categories')->onUpdate('restrict')->onDelete('restrict');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_product_categories', function (Blueprint $table){
            $table->dropForeign('products_product_categories_product_id_foreign');
            $table->dropForeign('products_product_categories_product_category_id_foreign');
        });

        Schema::dropIfExists('products_product_categories');
    }
}
