<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_media', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')
              ->constrained()
              ->onUpdate('restrict')
              ->onDelete('restrict');
            $table->enum('media_type', ['image', 'video']);
            $table->string('media_url', 2000);
            $table->boolean('is_main_image')->default(false);

            $table->foreignId('created_by');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('restrict')->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_media', function (Blueprint $table){
            // $table->dropForeign('product_id');
            $table->dropForeign('product_media_product_id_foreign');
            $table->dropForeign('product_media_created_by_foreign');
        });
        Schema::dropIfExists('product_media');
    }
}
