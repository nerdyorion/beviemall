<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 2000);
            $table->string('sku', 50);
            $table->string('slug', 2000)->nullable();
            $table->decimal('price', 10, 2);
            $table->decimal('price_without_tax', 10, 2);
            $table->foreignId('brand_id')->nullable()->constrained()->onUpdate('restrict')->onDelete('restrict');
            $table->float('weight_kg');
            $table->text('description')->nullable();
            $table->integer('quantity');
            $table->integer('quantity_low_mark')->nullable();
            $table->decimal('fixed_shipping_fee', 10, 2)->nullable();
            $table->boolean('is_free_shipping')->default(false);
            $table->string('page_meta_title', 2000)->nullable();
            $table->text('page_meta_description')->nullable();
            $table->boolean('is_featured')->default(false);
            $table->boolean('is_active')->default(true);

            $table->foreignId('created_by');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('restrict')->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table){
            $table->dropForeign(['brand_id']);
            $table->dropForeign('products_created_by_foreign');
        });

        Schema::dropIfExists('products');
    }
}
