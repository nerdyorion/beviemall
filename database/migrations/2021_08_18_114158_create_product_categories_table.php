<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categories', function (Blueprint $table) {
            $table->id();

            $table->foreignId('parent_id')->nullable();
            // $table->foreignId('brand_id')->constrained();
            $table->foreign('parent_id')->references('id')->on('product_categories')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->string('name', 255);
            $table->string('image_url', 2000)->nullable();
            $table->boolean('is_active')->default(true);

            $table->foreignId('created_by');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('restrict')->onDelete('restrict');

            $table->timestamps();


        });

        // Schema::table('product_categories', function (Blueprint $table){
        //     $table->foreign('parent_id')->references('id')->on('product_categories')
        //         ->onUpdate('restrict')
        //         ->onDelete('restrict');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_categories', function (Blueprint $table){
            // $table->dropForeign(['parent_id']);
            $table->dropForeign('product_categories_parent_id_foreign');
            $table->dropForeign('product_categories_created_by_foreign');
        });
        
        Schema::dropIfExists('product_categories');
    }
}
