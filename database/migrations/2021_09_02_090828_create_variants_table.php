<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variants', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained()->onUpdate('restrict')->onDelete('restrict');

            $table->enum('selection_type', ['swatch', 'radio', 'rectangle-list', 'dropdown']);
            $table->string('color_hex', 50)->nullable();
            $table->decimal('price', 10, 2)->nullable();
            $table->boolean('is_default')->default(false);

            $table->foreignId('created_by');
            $table->foreign('created_by')->references('id')->on('users')->onUpdate('restrict')->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('variants', function (Blueprint $table){
            // $table->dropForeign(['product_id']);
            $table->dropForeign('variants_product_id_foreign');
            $table->dropForeign('variants_created_by_foreign');
        });
        Schema::dropIfExists('variants');
    }
}
