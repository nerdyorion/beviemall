<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class OptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options')->insert([
            'name' => 'Color',
            'value' => 'Red',
            'created_by' => null,
            'created_at' => now(),
        ]);

        DB::table('options')->insert([
            'name' => 'Color',
            'value' => 'Blue',
            'created_by' => null,
            'created_at' => now(),
        ]);

        DB::table('options')->insert([
            'name' => 'Color',
            'value' => 'Green',
            'created_by' => null,
            'created_at' => now(),
        ]);

        DB::table('options')->insert([
            'name' => 'Size',
            'value' => 'S',
            'created_by' => null,
            'created_at' => now(),
        ]);

        DB::table('options')->insert([
            'name' => 'Size',
            'value' => 'M',
            'created_by' => null,
            'created_at' => now(),
        ]);

        DB::table('options')->insert([
            'name' => 'Size',
            'value' => 'L',
            'created_by' => null,
            'created_at' => now(),
        ]);

        DB::table('options')->insert([
            'name' => 'Size',
            'value' => 'XL',
            'created_by' => null,
            'created_at' => now(),
        ]);

        DB::table('options')->insert([
            'name' => 'Size',
            'value' => 'XXL',
            'created_by' => null,
            'created_at' => now(),
        ]);
    }
}